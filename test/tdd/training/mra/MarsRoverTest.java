package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	//Test user story 1
	public void testPlanet() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(7, 8);
		assertEquals(true, obstacle);

	}
	
	@Test
	//Test user story 2
	public void testLanding() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		String command = "";
		String status = rover.executeCommand(command);
		
		assertEquals("(0,0,N)", status);

	}
	
	@Test
	//Test user story 3
	public void testTurning() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		String command = "l";
		String status = rover.executeCommand(command);
		
		assertEquals("(0,0,W)", status);
		
		String command2 = "r";
		String status2 = rover.executeCommand(command2);
		
		assertEquals("(0,0,N)", status2);
		
		String command3 = "r";
		String status3 = rover.executeCommand(command3);
		
		assertEquals("(0,0,E)", status3);

	}
	
	@Test
	//Test user story 4
	public void testMovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		String command = "f";
		String status = rover.executeCommand(command);
		assertEquals("(0,1,N)", status);
		
		String command2 = "r";
		String status2 = rover.executeCommand(command2);
		assertEquals("(0,1,E)", status2);
		
		String command3 = "f";
		String status3 = rover.executeCommand(command3);
		
		assertEquals("(1,1,E)", status3);

	}
	
	@Test
	//Test user story 5
	public void testMovingBackward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		String command = "f";
		String status = rover.executeCommand(command);
		assertEquals("(0,1,N)", status);
		
		String command2 = "b";
		String status2 = rover.executeCommand(command2);
		assertEquals("(0,0,N)", status2);

	}

	@Test
	//Test user story 6
	public void testMovingCombined() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 12, planetObstacles);
		
		String command = "ffrff";
		String status = rover.executeCommand(command);
		assertEquals("(2,2,E)", status);
	}

	@Test
	//Test user story 7
	public void testWrapping() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String command = "b";
		String status = rover.executeCommand(command);
		assertEquals("(0,9,N)", status);	
	}

	@Test
	//Test user story 8
	public void testSingleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);	
		
		String command = "ffrfff";
		String status = rover.executeCommand(command);
		
		assertEquals("(1,2,E)(2,2)", status);	
	}
	
	@Test
	//Test user story 9
	public void testMultipleObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String command = "ffrfffrflf";	
		String status = rover.executeCommand(command);

		assertEquals("(1,1,E)(2,1)(2,2)", status);	
	}
	
	@Test
	//Test user story 10
	public void testWrappingAndObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		String command = "b";
		String status = rover.executeCommand(command);
		
		assertEquals("(0,0,N)(0,9)", status);	
	}
	
	@Test
	//Test user story 11
	public void testTour() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		
		MarsRover rover = new MarsRover(6, 6, planetObstacles);
		
		String command = "ffrfffrbbblllfrfrbbl";
		String status = rover.executeCommand(command);
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", status);
		
		
		/*
		//case: the rover doesn't meet all obstacles
		String command2 = "ffrfffrbbblllf";
		String status2 = rover.executeCommand(command2);
		assertEquals("(1,5,W)(2,2)(0,5)", status2);	
		*/
	}
	

}
